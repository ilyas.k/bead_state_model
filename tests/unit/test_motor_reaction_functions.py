from typing import List
from bead_state_model.system_setup import (_get_forward_neighbor2,
                                           _neighbor_leads_to_filament_head)

def test_get_forward_neighbor2():
    n_beads = 10
    fil = ['tail'] + ['core']*(n_beads-2) + ['head', 'motor']

    # ==========================================================================
    # ---> motor at tail
    fil[1] = 'motor'
    top = MockTopology(fil)
    for i in range(n_beads-1):
        top.add_edge(i, i+1)
    top.add_edge(1, n_beads)

    # check tail: tail can have only one neighbor, which
    # has to be the forward neighbor
    v = _get_forward_neighbor2(top, top.vertices[0])
    assert v.particle_type != 'tail'
    assert v.particle_index == 1
    
    v = _get_forward_neighbor2(top, top.vertices[1])
    assert 2 == v.particle_index

    # check motor: can't have forward neighbor, is front of filament
    v = _get_forward_neighbor2(top, top.vertices[n_beads-1])
    assert v is None
    # <---
    # ==========================================================================
    # ---> motor in center
    fil[1] = 'core'
    fil[3] = 'motor'
    top = MockTopology(fil)
    for i in range(n_beads-1):
        top.add_edge(i, i+1)
    top.add_edge(3, n_beads)

    v = _get_forward_neighbor2(top, top.vertices[3])
    assert 4 == v.particle_index
    # <---
    # ==========================================================================
    # ---> two consecutive motors => can't determine forward neighbor
    fil[4] = 'motor'
    top = MockTopology(fil)
    for i in range(n_beads-1):
        top.add_edge(i, i+1)
    top.add_edge(3, n_beads)

    v = _get_forward_neighbor2(top, top.vertices[3])
    assert v is None
    # <---
    # ==========================================================================
    # ---> motor at head => has no forward neighbor
    fil[3] = 'core'
    fil[4] = 'core'
    fil[n_beads-2] = 'motor'
    top = MockTopology(fil)
    for i in range(n_beads-1):
        top.add_edge(i, i+1)
    top.add_edge(3, n_beads)

    v = _get_forward_neighbor2(top, top.vertices[n_beads-2])
    assert v is None
    

    
    

def test_neighbor_leads_to_filament_head():

    n_beads = 10
    fil = ['tail'] + ['core']*(n_beads-2) + ['head'] + ['motor']
    # ==========================================================================
    # ---> motor at tail
    fil[1] = 'motor'
    top = MockTopology(fil)
    for i in range(n_beads-1):
        top.add_edge(i, i+1)
    top.add_edge(1, n_beads)

    # expect ValueError: source vertex needs to be of type motor
    try:
        _neighbor_leads_to_filament_head(top, top.vertices[0], top.vertices[1])
        assert False
    except ValueError:
        assert True

    # dead end
    assert not _neighbor_leads_to_filament_head(top, top.vertices[1], top.vertices[0])
    # direction of another motor -> likely leads to another filament => not considered fwd_neighbor
    assert not _neighbor_leads_to_filament_head(top, top.vertices[1], top.vertices[n_beads])
    # correct forward direction
    assert _neighbor_leads_to_filament_head(top, top.vertices[1], top.vertices[2])
    # <---
    # ==========================================================================
    # ---> motor in center
    fil[1] = 'core'
    fil[3] = 'motor'
    top = MockTopology(fil)
    for i in range(n_beads-1):
        top.add_edge(i, i+1)
    top.add_edge(3, n_beads)

    assert _neighbor_leads_to_filament_head(top, top.vertices[3], top.vertices[4])
    assert not _neighbor_leads_to_filament_head(top, top.vertices[3], top.vertices[2])
    assert not _neighbor_leads_to_filament_head(top, top.vertices[3], top.vertices[n_beads])
    # <---
    # ==========================================================================
    # ---> two consecutive motors => can't determine forward neighbor
    fil[4] = 'motor'
    top = MockTopology(fil)
    for i in range(n_beads-1):
        top.add_edge(i, i+1)
    top.add_edge(3, n_beads)
    assert not _neighbor_leads_to_filament_head(top, top.vertices[3], top.vertices[4])
    

class MockTopology:

    def __init__(self, particle_types: List[str]):
        self.vertices = []
        for p in particle_types:
            v = MockVertex(self, len(self.vertices), p)
            self.vertices.append(v)
        self.edges = []

    def particle_type_of_vertex(self, v):
        return v.particle_type

    def add_edge(self, idx1, idx2):
        self.edges.append((idx1, idx2))
        self.vertices[idx1].neighbors_list.append(self.vertices[idx2])
        self.vertices[idx2].neighbors_list.append(self.vertices[idx1])


class MockVertex:

    def __init__(self, parent_top, idx: int, ptype: str):
        self.parent_topology = parent_top
        self.particle_index = idx
        self.particle_type = ptype
        self.neighbors_list = []

    def get(self):
        return self

    def neighbors(self):
        return self.neighbors_list

    def __iter__(self):
        return self.neighbors_list.__iter__()

    def __str__(self):
        s = "MockVertex {}, type {}, neighbors [{}]"
        neighbor_s = ",".join([str(n.particle_index) for n in self.neighbors_list])
        return s.format(self.particle_index,
                        self.particle_type,
                        neighbor_s)

    def __repr__(self):
        return self.__str__()
        
