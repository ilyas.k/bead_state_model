from typing import Tuple, List
import numpy as np
from bead_state_model.components import Filament, LinksArray
from bead_state_model.system_setup import (_get_connected_filaments, _get_motor_edges,
                                           _filaments_to_topology_inits)


def test_get_connected_filaments():
    # case 4 isolated filaments
    filaments, links = _setup_filaments()

    for f in range(len(filaments)):
        fil = filaments[f]
        connected = _get_connected_filaments(fil.motors, links,
                                             filaments, f + 1, [])
        assert len(connected) == 0

    # case 2 connected filaments, 2 isolated
    filaments, links = _setup_2_connected_2_isolated_filaments()

    fil = filaments[0]
    connected = _get_connected_filaments(fil.motors, links, filaments,
                                         start=1, skip=[])
    assert 1 in connected
    assert len(connected) == 1

    for f in range(1, len(filaments)):
        fil = filaments[f]
        connected = _get_connected_filaments(fil.motors, links,
                                             filaments, f + 1, [])
        assert len(connected) == 0

    # case 3 connected, 1 isolated
    filaments, links = _setup_3_connected_1_isolated_filaments()

    fil = filaments[0]
    connected = _get_connected_filaments(fil.motors, links, filaments,
                                         start=1, skip=[])
    assert len(connected) == 2
    assert 1 in connected
    assert 2 in connected

    fil = filaments[1]
    connected = _get_connected_filaments(fil.motors, links, filaments,
                                         start=2, skip=[])
    assert len(connected) == 1
    assert 2 in connected

    # with skip, to account for previously visited filament (since fil2
    # is already part of the topology starting with fil0)
    connected = _get_connected_filaments(fil.motors, links, filaments,
                                         start=2, skip=[2])
    assert len(connected) == 0

    # case 4 connected (cyclic connection)
    filaments, links = _setup_4_connected_filaments()

    fil = filaments[0]
    s = []
    connected = _get_connected_filaments(fil.motors, links, filaments,
                                         start=1, skip=s)
    assert len(s) == 3
    assert 1 in s
    assert 2 in s
    assert 3 in s
    assert len(connected) == 3
    assert 1 in connected
    assert 2 in connected
    assert 3 in connected

    for f in range(1, len(filaments)):
        fil = filaments[f]
        connected = _get_connected_filaments(fil.motors, links, filaments,
                                             start=f + 1, skip=[1, 2, 3])
        assert len(connected) == 0


def test_get_motor_edges():
    filaments, links = _setup_2_connected_2_isolated_filaments()
    fil_connected = [0, 1]
    beads = []
    for f in fil_connected:
        beads += filaments[f].items

    edges = _get_motor_edges([filaments[f] for f in fil_connected], links, beads)

    # connected_for_assert = [(4, 18)]

    assert len(edges) == 1
    assert (4, 18) in edges

    # try with filaments in swapped order
    beads = []
    for f in fil_connected[::-1]:
        beads += filaments[f].items
    edges = _get_motor_edges([filaments[f] for f in fil_connected], links, beads)

    assert (14, 8) in edges

    # setup with 4 connected filaments
    filaments, links = _setup_4_connected_filaments()

    fil_connected = [0, 1, 2, 3]
    beads = []
    for f in fil_connected:
        beads += filaments[f].items

    edges = _get_motor_edges([filaments[f] for f in fil_connected], links, beads)

    assert len(edges) == 4
    assert (4, 18) in edges
    assert (14, 23) in edges
    assert (28, 32) in edges
    assert (2, 38) in edges

    # try with mixed order
    fil_connected = [1, 2, 3, 0]
    beads = []
    for f in fil_connected:
        beads += filaments[f].items

    edges = _get_motor_edges([filaments[f] for f in fil_connected], links, beads)
    assert len(edges) == 4
    assert (34, 8) in edges or (8, 34) in edges, edges
    assert (4, 13) in edges or (13, 4) in edges
    assert (18, 22) in edges or (22, 18) in edges
    assert (32, 28) in edges or (28, 32) in edges


def test_filaments_to_topology_inits():
    # first test with 4 separate filaments
    filaments, links = _setup_filaments()

    # we need some positions for the function ...
    # values do not really matter
    positions = np.zeros((len(links), 3))
    positions[:, 1] = np.arange(len(links))

    map_fid, map_bid, fil_new, links_new, topologies = \
        _filaments_to_topology_inits(filaments, positions, links)

    # maps should be ordered in this case
    assert (np.arange(len(map_fid)) == map_fid).all()
    assert (np.arange(len(map_bid)) == map_bid).all()
    assert len(filaments) == len(fil_new)
    assert (links == links_new).all()
    assert len(topologies) == 4
    # topologies here are tuples with 3 entries
    # item [1] is positions array
    assert (topologies[0][1] == positions[:10]).all()
    assert (topologies[1][1] == positions[10:20]).all()
    assert (topologies[2][1] == positions[20:30]).all()
    assert (topologies[3][1] == positions[30:]).all()

    for t in range(len(topologies)):
        top = topologies[t]
        assert len(top[2]) == 9
        for i in range(9):
            assert (i, i + 1) in top[2]
        assert top[0][0] == 'tail'
        assert top[0][-1] == 'head'
        for i in range(1, 9):
            assert top[0][i] == 'core'

    # more interesting case: 2 connected filaments

    filaments, links = _setup_filaments()

    # connect fil1 and fil3 at beads 13 and 33
    fil1 = filaments[1]
    fil3 = filaments[3]

    fil1.motors.append(13)
    fil3.motors.append(33)

    links[13, 2] = 33
    links[33, 2] = 13

    map_fid, map_bid, fil_new, links_new, topologies = \
        _filaments_to_topology_inits(filaments, positions, links)

    # in this case this should cause a reordering of filaments 2 and 3, since
    # filament 3 is connected to a filament with lower index, namely filament 1

    assert 0 == map_fid[0]
    assert 1 == map_fid[1]
    assert 3 == map_fid[2]
    assert 2 == map_fid[3]
    assert (np.arange(20) == map_bid[:20]).all()
    assert (np.arange(20, 30) == map_bid[30:]).all()
    assert (np.arange(30, 40) == map_bid[20:30]).all()
    assert (links[:, :2] == links_new[:, :2]).all(), (links, links_new)
    assert 23 == links_new[13, 2]
    assert 13 == links_new[23, 2]

    assert 3 == len(topologies)
    # this is an individual filament
    top0 = topologies[0]

    # check edges
    assert len(top0[2]) == 9
    for i in range(9):
        assert (i, i + 1) in top0[2]

    # check species list
    assert top0[0][0] == 'tail'
    assert top0[0][-1] == 'head'
    for i in range(1, 9):
        assert top0[0][i] == 'core'

    assert (top0[1] == positions[:10]).all()

    # this is two filaments connected via one motor
    top1 = topologies[1]

    assert (top1[1][:10] == positions[10:20]).all()
    assert (top1[1][10:] == positions[30:]).all()

    # check edges
    assert len(top1[2]) == 19, (len(top1[2]), top1[2])
    for i in range(9):
        assert (i, i + 1) in top1[2]
        assert (i + 10, i + 11) in top1[2], top1[2]
    assert (3, 13) in top1[2]

    # check species list
    assert top1[0][0] == 'tail'
    assert top1[0][-1] == 'head'
    for i in range(1, 9):
        if i == 3:
            assert top1[0][i] == 'motor'
            assert top1[0][i + 10] == 'motor'
            continue
        assert top1[0][i] == 'core'
        assert top1[0][i + 10] == 'core'

    # this is a separate filament again
    top2 = topologies[2]

    assert len(top2[2]) == 9
    for i in range(9):
        assert (i, i + 1) in top2[2]

    # check species list
    assert top2[0][0] == 'tail'
    assert top2[0][-1] == 'head'
    for i in range(1, 9):
        assert top2[0][i] == 'core'

    assert (top2[1] == positions[20:30]).all()


def _setup_filaments() -> Tuple[List[Filament], LinksArray]:
    n_fil = 4
    n_beads = 10

    links = np.full((n_fil * n_beads, 3), -1, dtype=int)

    for f in range(n_fil):
        links[n_beads * f + 1: n_beads * (f + 1), 0] = np.arange(n_beads - 1) + f * n_beads
        links[n_beads * f: n_beads * (f + 1) - 1, 1] = np.arange(1, n_beads) + f * n_beads

    filaments = []
    for id_, tail in enumerate(range(0, n_beads * n_fil, n_beads)):
        filaments.append(Filament(id_, tail, links))

    return filaments, links


def _setup_2_connected_2_isolated_filaments() -> Tuple[List[Filament], LinksArray]:
    filaments, links = _setup_filaments()

    # connect fil 0 at bead 4 with
    # fil 1 at bead 18
    f0 = filaments[0]
    f1 = filaments[1]

    f0.motors.append(4)
    f1.motors.append(18)

    links[4, 2] = 18
    links[18, 2] = 4

    return filaments, links


def _setup_3_connected_1_isolated_filaments() -> Tuple[List[Filament], LinksArray]:
    filaments, links = _setup_filaments()

    # connect fil 0 at bead 4 with
    # fil 1 at bead 18
    f0 = filaments[0]
    f1 = filaments[1]

    f0.motors.append(4)
    f1.motors.append(18)

    links[4, 2] = 18
    links[18, 2] = 4

    # connect fil 1 at bead 14 with
    # fil 2 at bead 23
    f2 = filaments[2]

    f1.motors.append(14)
    f2.motors.append(23)

    links[14, 2] = 23
    links[23, 2] = 14

    return filaments, links


def _setup_4_connected_filaments() -> Tuple[List[Filament], LinksArray]:
    filaments, links = _setup_filaments()

    # connect fil 0 at bead 4 with
    # fil 1 at bead 18
    f0 = filaments[0]
    f1 = filaments[1]

    f0.motors.append(4)
    f1.motors.append(18)

    links[4, 2] = 18
    links[18, 2] = 4

    # connect fil 1 at bead 14 with
    # fil 2 at bead 23
    f2 = filaments[2]

    f1.motors.append(14)
    f2.motors.append(23)

    links[14, 2] = 23
    links[23, 2] = 14

    # connect fil 2 at bead 28 with
    # fil 3 at bead 32

    f3 = filaments[3]

    f2.motors.append(28)
    f3.motors.append(32)

    links[28, 2] = 32
    links[32, 2] = 28

    # connect fil 3 at bead 38
    # with fil 0 at bead 2

    f0.motors.append(2)
    f3.motors.append(38)

    links[2, 2] = 38
    links[38, 2] = 2

    return filaments, links
