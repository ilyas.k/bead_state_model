from tempfile import TemporaryDirectory
import os

import numpy as np

from bead_state_model.periodic_boundaries import get_minimum_image_vector
from tests.acceptance.conftest import create_initial_state, run_equilibration, run_simulation, read_trajectories

N_FILAMENTS = 10
N_BEADS_PER_FILAMENT = 25
DT = 0.006
BOX = np.array([30., 30., 30.])


def test_execute_and_read_consecutive_simulations():
    with TemporaryDirectory() as tempdir:

        kernels = ['SingleCPU'] * 2 + ['CPU'] * 2

        for i, kernel in enumerate(kernels):
            init_dir = os.path.join(tempdir, f'init{i}')
            equil_dir = os.path.join(tempdir, f'equil{i}')
            out_dir = os.path.join(tempdir, f'out{i}')

            create_initial_state(init_dir, BOX, N_BEADS_PER_FILAMENT, N_FILAMENTS)
            run_equilibration(init_dir, equil_dir, kernel, BOX, N_BEADS_PER_FILAMENT, N_FILAMENTS)
            run_simulation(equil_dir, out_dir, kernel,
                           dt=DT, n_steps=100, observation_interval=1,
                           box=BOX,
                           n_beads_per_filament=N_BEADS_PER_FILAMENT,
                           n_filaments=N_FILAMENTS)
            traj = read_trajectories(out_dir)
            _assert_no_large_displacements(traj)


def _assert_no_large_displacements(trajectory: np.ndarray):
    diff = get_minimum_image_vector(trajectory[1:], trajectory[:-1], np.array(BOX))
    d = np.sqrt((diff ** 2).sum(2))
    threshold = np.sqrt(DT * 2) * 50
    below_threshold = (d - threshold) < 0.0
    assert below_threshold.all(), len(below_threshold.flatten()) - below_threshold.sum()
