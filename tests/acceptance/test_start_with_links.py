import os.path
from tempfile import TemporaryDirectory
from typing import Tuple, List, Dict, Any

import numpy as np
import readdy
import toml

from bead_state_model import Simulation, SimulationParameters, BaseSetupHandler
from bead_state_model.data_reader import DataReader

_HERE = os.path.split(__file__)[0]
_INIT_STATE_FOLDER = os.path.join(_HERE, 'resources', 'initial_state_with_links')


def test_start_with_links_static():
    parameters = SimulationParameters.load_parameters_from_simulation_toml(
        os.path.join(_INIT_STATE_FOLDER, 'config.toml')
    )
    links = _run_simulation(parameters)
    _assert_motors_were_created_and_number_does_not_change(links)


def test_start_with_links_dynamic():
    parameters = SimulationParameters.load_parameters_from_simulation_toml(
        os.path.join(_INIT_STATE_FOLDER, 'config.toml')
    )
    parameters.rate_motor_bind = 0.5
    parameters.rate_motor_unbind = 1.0
    links = _run_simulation(parameters)
    _assert_number_of_links_is_positive(links)


def _run_simulation(parameters):
    with TemporaryDirectory() as tempdir:
        s = Simulation(
            output_folder=tempdir,
            parameters=parameters,
            interaction_setup_handler=_SimulationSetupHandler()
        )

        filament_coordinates, links = _load_initial_state()

        s.add_filaments_via_arrays(filament_coordinates, links)

        n_steps = 30
        obs_interval = 1
        s.run(n_steps, 0.005, obs_interval, mute=True)

        dr = DataReader(os.path.join(tempdir, 'data.h5'))
        traj = dr.read_particle_positions()
        links = dr.get_links_all()
        assert not np.isnan(traj).any()
    return links


def _load_initial_state() -> Tuple[np.ndarray, np.ndarray]:
    fil_file = os.path.join(_INIT_STATE_FOLDER, 'coordinates_filaments.npy')
    links_file = os.path.join(_INIT_STATE_FOLDER, 'links.npy')
    return np.load(fil_file), np.load(links_file)


def _assert_motors_were_created_and_number_does_not_change(links: List[np.ndarray]):
    n_links = None
    for l in links:
        n = (l[:, 2] != -1).sum()
        assert n > 0
        if n_links is None:
            n_links = n
        assert n_links == n


def _assert_number_of_links_is_positive(links: List[np.ndarray]):
    for l in links:
        assert l[:, 2].sum() >= 0


def _setup_z_potential(system: readdy.ReactionDiffusionSystem, box_potential: Dict[str, Any]):
    for p in ['head', 'tail', 'core', 'motor']:
        system.potentials.add_box(
            particle_type=p, force_constant=box_potential['k'],
            origin=box_potential['origin'], extent=box_potential['extent']
        )


def _setup_spherical_exclusion(
        system: readdy.ReactionDiffusionSystem,
        parameters: Dict[str, Any]
):
    position = np.load(os.path.join(_INIT_STATE_FOLDER, 'coordinates_non_filament_particles.npy'))[0]
    radius = parameters['radius']
    repulsion = parameters['repulsion']

    for p in ['head', 'tail', 'core', 'motor']:
        system.potentials.add_sphere(
            p, repulsion, position, radius + 0.5, inclusion=False
        )


class _SimulationSetupHandler(BaseSetupHandler):

    @staticmethod
    def from_config_dict(d: Dict[str, Any]) -> 'BaseSetupHandler':
        return _SimulationSetupHandler()

    def to_config_dict(self) -> Dict[str, Any]:
        return {}

    def __call__(self, system: readdy.ReactionDiffusionSystem):
        system.periodic_boundary_conditions = [True, True, False]
        with open(os.path.join(_INIT_STATE_FOLDER, 'config.toml'), 'r') as fp:
            config = toml.load(fp)
        _setup_z_potential(system, config['box_potential'])
        _setup_spherical_exclusion(system, config['indenter'])