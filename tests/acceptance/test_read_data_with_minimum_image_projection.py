import os
from tempfile import TemporaryDirectory

import numpy as np

from bead_state_model.data_reader import DataReader
from tests.acceptance.conftest import create_initial_state, run_equilibration, run_simulation

N_FILAMENTS = 1
N_BEADS_PER_FILAMENT = 30
DT = 0.006
BOX = np.array([15., 15., 15.])


def test_read_data_with_minimum_image_projection():
    with TemporaryDirectory() as tempdir:
        kernel = 'SingleCPU'
        init_dir = os.path.join(tempdir, 'init')
        out_dir = os.path.join(tempdir, f'out')
        create_initial_state(init_dir, BOX, N_BEADS_PER_FILAMENT, N_FILAMENTS)
        run_simulation(init_dir, out_dir, kernel,
                       dt=DT, n_steps=100, observation_interval=1,
                       box=BOX,
                       n_beads_per_filament=N_BEADS_PER_FILAMENT,
                       n_filaments=N_FILAMENTS)

        traj = _read_trajectories_minimum_image(out_dir)
        _assert_no_coordinates_exceed_box(traj)


def _read_trajectories_minimum_image(out_dir: str) -> np.ndarray:
    dr = DataReader(os.path.join(out_dir, 'data.h5'))
    traj = dr.read_particle_positions(minimum_image=True)
    return traj


def _assert_no_coordinates_exceed_box(trajectories: np.ndarray):
    for i in range(len(BOX)):
        assert (trajectories[:, :, i] <= BOX[i]/2).all(), (i, trajectories[:, :, i].max())
        assert (trajectories[:, :, i] >= -BOX[i]/2).all(), (i, trajectories[:, :, i].min(),
                                                            trajectories[:, :, i].max())


