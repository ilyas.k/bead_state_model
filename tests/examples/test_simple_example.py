import os
from tempfile import TemporaryDirectory

from bead_state_model.data_reader import DataReader
from tests.examples.conftest import AnalysisScript, ScriptFiles, copy_scripts_to_tempdir
from tests.examples.conftest import EXAMPLE_ROOT, execute_function_from_script

DIR = os.path.join(EXAMPLE_ROOT, 'simple_example')


def test_simple_example():
    tempdir = TemporaryDirectory()
    scripts = ScriptFiles(
        'create_network.py',
        'run_simulation.py',
        [AnalysisScript('export_xyz.py'),
         AnalysisScript('plot_10_random_filaments_2D_with_analyser.py', 'main'),
         AnalysisScript('plot_length_distributions.py', 'main', {'frames': [0, 20]})]
    )
    copy_scripts_to_tempdir(tempdir, DIR, scripts)
    os.chdir(tempdir.name)
    exec(open(scripts.create_network, 'rt').read())
    execute_function_from_script(scripts.run_simulation, 'main')
    dr = DataReader('simulation_output/data.h5')
    coords = dr.get_filament_coordinates(minimum_image=False)
    assert (21, 4500, 3) == coords.shape

    for ana in scripts.analysis_scripts:
        ana.execute()



