import os.path
from tempfile import TemporaryDirectory

from tests.examples.conftest import ScriptFiles, AnalysisScript, copy_scripts_to_tempdir
from tests.examples.conftest import EXAMPLE_ROOT, execute_function_from_script

DIR = os.path.join(EXAMPLE_ROOT, 'tune_persistence_length')


def test_tune_persistence_length():
    tempdir = TemporaryDirectory()
    scripts = ScriptFiles(
        'run_create_filament.py',
        'run_simulations.py',
        [AnalysisScript('plot_cos_theta.py', 'main'),
         AnalysisScript('plot_segment_length_dist.py', 'main')],
        config_file='default_params.toml'
    )
    copy_scripts_to_tempdir(tempdir, DIR, scripts)
    os.chdir(tempdir.name)
    scripts.override_run_parameters_in_config(n_steps=1000, observation_interval=10)
    execute_function_from_script(scripts.create_network, 'main', {'n_proc': 1})
    execute_function_from_script(scripts.run_simulation, 'main', {'n_proc': 1})

    for ana in scripts.analysis_scripts:
        ana.execute()
