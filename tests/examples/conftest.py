from pathlib import Path
import os
import shutil
from dataclasses import dataclass
from importlib import util
from typing import Optional, Dict, Any, List

import toml

HERE = os.path.split(__file__)[0]
EXAMPLE_ROOT = os.path.join(HERE, '../../examples')


def execute_function_from_script(
        script_file: str,
        function_name: str,
        kwargs: Optional[Dict[str, Any]] = None
):
    module_folder = os.path.split(script_file)[0]
    spec = util.spec_from_file_location(
        'module',
        script_file,
        submodule_search_locations=[module_folder, '.']
    )
    module = spec.loader.load_module()
    func = getattr(module, function_name)
    kwargs = kwargs if kwargs is not None else {}
    func(**kwargs)


@dataclass
class AnalysisScript:
    file_name: str
    main_function: Optional[str] = None
    function_kwargs: Optional[Dict[str, Any]] = None

    def execute(self):
        print(os.getcwd(), __file__, self.file_name)
        if self.main_function is None:
            exec(open(self.file_name, 'rt').read())
            return
        spec = util.spec_from_file_location(
            'module',
            self.file_name,
        )
        module = spec.loader.load_module()
        func = getattr(module, self.main_function)
        kwargs = {} if self.function_kwargs is None else self.function_kwargs
        func(**kwargs)


@dataclass
class ScriptFiles:
    create_network: str
    run_simulation: str
    analysis_scripts: List['AnalysisScript']
    config_file: Optional[str] = None
    run_equilibration: Optional[str] = None

    def __iter__(self):
        yield self.create_network
        if self.run_equilibration is not None:
            yield self.run_equilibration
        yield self.run_simulation
        if self.config_file is not None:
            yield self.config_file
        for ana in self.analysis_scripts:
            yield ana.file_name

    def override_run_parameters_in_config(self, **params: Any):
        self.override_parameters_in_config('run', **params)

    def override_parameters_in_config(self, category: str, **params: Any):
        with open(self.config_file, 'rt') as fp:
            config = toml.load(fp)
        config[category].update(params)
        with open(self.config_file, 'wt') as fp:
            toml.dump(config, fp)


def copy_scripts_to_tempdir(tempdir, source_dir: str, scripts: 'ScriptFiles'):
    for f in scripts:
        src = Path(source_dir) / f
        dst = Path(tempdir.name) / f
        dst.parent.mkdir(exist_ok=True)
        shutil.copy(src, dst)
