import os
from tempfile import TemporaryDirectory
import sys

from tests.examples.conftest import (
    ScriptFiles, copy_scripts_to_tempdir, EXAMPLE_ROOT,
    execute_function_from_script, AnalysisScript
)

DIR = os.path.join(EXAMPLE_ROOT, 'passive_microrheology')


def test_microrheology():
    tempdir = TemporaryDirectory()
    sys.path.append(tempdir.name)
    scripts = ScriptFiles(
        'create_networks.py',
        'run_simulations.py',
        analysis_scripts=[AnalysisScript('analysis/export_xyz.py', 'main'),
                          AnalysisScript('analysis/plot_microrheo_bead_trajectory.py',
                                         'main')],
        config_file='default_params.toml',
        run_equilibration='run_equilibrations.py'
    )
    copy_scripts_to_tempdir(tempdir, DIR, scripts)
    os.chdir(tempdir.name)
    scripts.override_parameters_in_config('create', n_filaments=200, n_beads_per_filament=15)
    execute_function_from_script(scripts.create_network, 'main', {'n_proc': 1})
    execute_function_from_script(scripts.run_equilibration, 'main', {'n_processes': 1})
    execute_function_from_script(scripts.run_simulation, 'main', {'n_processes': 1})

    for ana in scripts.analysis_scripts:
        ana.execute()
