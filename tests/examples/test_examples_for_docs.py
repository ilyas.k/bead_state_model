import os
import shutil
from tempfile import TemporaryDirectory
from typing import Optional, Tuple

import numpy as np

from bead_state_model.data_reader import DataReader
from tests.examples.conftest import execute_function_from_script

HERE = os.path.split(__file__)[0]
EXAMPLE_ROOT = os.path.join(HERE, '../../examples')
EXAMPLE_FOR_DOCS_ROOT = os.path.join(EXAMPLE_ROOT, 'for_docs')


def test_simple_network():
    sim_file = 'run_sim.py'
    _, coords = _run_sim_and_read_coords(sim_file)
    assert (21, 1000, 3) == coords.shape


def test_network_with_external_particles():
    sim_file = 'run_sim_with_external_topology.py'
    coords_external, coords_filaments = _run_sim_and_read_coords(sim_file, 'run_sim')
    assert (21, 1000, 3) == coords_filaments.shape
    assert (21, 2, 3) == coords_external.shape


def _run_sim_and_read_coords(
        sim_file: str,
        function_name: Optional[str] = None
) -> Tuple[np.ndarray, np.ndarray]:
    tempdir = TemporaryDirectory()
    files = ['create_network.py', sim_file]
    _copy_scripts_to_tempdir(files, tempdir)
    os.chdir(tempdir.name)
    print("Creating network.")
    exec(open('create_network.py', 'rt').read())
    print("Running sim.")
    if function_name is None:
        exec(open(sim_file, 'rt').read())
    else:
        execute_function_from_script(sim_file, function_name)
    print(os.listdir('simulation_output'))
    dr = DataReader('simulation_output/data.h5')
    coords_ext = dr.get_non_filament_coordinates(minimum_image=False)
    coords_fil = dr.get_filament_coordinates(minimum_image=False)
    return coords_ext, coords_fil


def _copy_scripts_to_tempdir(files, tempdir):
    for f in files:
        src = os.path.join(EXAMPLE_FOR_DOCS_ROOT, f)
        target = os.path.join(tempdir.name, f)
        shutil.copy(src, target)
