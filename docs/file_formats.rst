File Formats
============

Simulation Output
-----------------

The simulations produce usually 2 important files in which it places the
simulation data, `data.h5`_ and `links.h5`_.
**Note that these names are the defaults I use in the examples.** But they
can be changed by the user, so the names of your output files
can be different.

As a user of ``bead_state_model``, you mainly want to stick to using
:class:`bead_state_model.data_reader.DataReader`, which provides
methods to load most data relevant for analysis.

Some of the data reading is quite slow. Reading trajectories from ``data.h5``
via ``readdy`` can take very long and can take up a lot of memory for large
simulated systems. I created a separate project for reading data
of actomyosin simulations, that can also handle data from ``bead_state_model``.
That project is
called `actomyosin_analyser <https://gitlab.gwdg.de/ikuhlem/actomyosin_analyser>`_.
Usage of the essential class
:class:`actomyosin_analyser.analysis.analyser.Analyser` is also demonstrated
in :ref:`this example <tune-l-p>`. The :class:`Analyser` class automatically stores
most data that it reads from ``data.h5`` in nicer formats, and will be
much faster when you have to repeat some analyses.

``data.h5``
...........

This file is the *normal* readdy output file. Consult the
`documentation of readdy <https://readdy.github.io/index.html>`_ if you want
to add observables and you want toread out properties that are not accessible via
:class:`bead_state_model.data_reader.DataReader`.

``links.h5``
............

This file is created by :class:`bead_state_model.filament_handler.FilamentHandler`.
It contains data on filaments (which beads constitute which filaments) and
the links between filaments.

``force_monitor.log``
.....................

In some examples I also
want to monitor whether beads feel a force that exceeds a certain
threshold. These incidents are counted for each frame and saved in
file ``force_monitor.log``.

Split into parts
................

In some examples there are multiple subfolders in the output folder that are named
``part_0000``, ``part_0001``, etc.. This means that the simulation is split into
multiple parts. The normal output files will then be placed into these part subfolders.
You usually don't need to think about that, as :class:`bead_state_model.data_reader.DataReader`
can handle split data. See this :ref:`example <example bead in network>` for splitting output
and how to analyse it.

Bead File
---------

A bead file defines all the beads in the system. In ``bead_state_model``
it is used to save pre-generated networks (generated via python scripts)
and load those networks as initial states for simulations.

Each bead is defined via 7 properties, which are stored in the 7 columns of a bead file:

#. Bead ID,
#. x position,
#. y position,
#. z position,
#. Bead ID of previous bead (in filament, when you go from tail to head),
#. Bead ID of next bead,
#. Bead ID of bead in other filament this bead is bound to. This would make the current
   bead (and the linked bead) a motor or cross-link.

Each row defines one bead. The last 3 columns are usually being loaded into a
numpy array as well. The information stored in these 3 columns is described
in more detail in the following section

``LinksArray``
..............

A ``LinksArray`` contains information on all the bonds between
filamentous beads. A row contains bond information of the bead
with the same ID as the index of that row. E.g. row 5 contains
connection information of bead 5:

.. code::

   >>> print(links[5])
   [9, -1, -1]

The output means that bead 5 has one previous neighbor, which is the bead
with ID 9. The second entry is the link to the following neighbor, but the
-1 indicates, that bead 5 does not have a following neighbor bead. I.e.
it has to be the head of the filament. The third entry is a link to a
bead of another filament. It is -1 here as well (heads can't form connections
to other filaments, so -1 is the only option for a head bead like bead 5).

We can use this information to go along the filament of which bead 5 is the head.
The previous bead is bead 9. It could have information like this stored in the
``LinkArray``:

.. code::

   >>> print(links[9])
   [12, 5, 14]

This means the bead previous to bead 9 in the filament is bead 12, following it is
bead 5 (as we knew before from the information of bead 5). The third entry is 14,
meaning that bead 9 has a cross-filament connection to bead 14. This makes
bead 9 (as well as bead 14) a motor or a cross-link.
