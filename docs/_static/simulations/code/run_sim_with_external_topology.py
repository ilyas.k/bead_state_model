from typing import Dict, Any

import numpy as np
from readdy import ReactionDiffusionSystem
from bead_state_model import Simulation, SimulationParameters, BaseSetupHandler


def run_sim():
    parameters = SimulationParameters(
        n_beads_max=20000,
        box_size=np.array([20., 20., 20.]),
        k_bend=26.0,
        k_stretch=20.0,
    )

    r = 2.0
    sim = Simulation(
        'simulation_output',
        parameters,
        interaction_setup_handler=MoleculeSetupHandler(r)
    )

    # 'molecule' topology and 'particle' species
    # are defined in class MoleculeSetupHandler below.
    # Add molecule around [0, 0, 0] coordinates:
    coordinates = np.array([
        [r, 0., 0.],
        [-r, 0., 0.]
    ])
    sim.add_non_filament_topology(
        'molecule',
        ['particle', 'particle'],
        coordinates=coordinates,
        graph_edges=[(0, 1)]
    )
    # Requires that the file ``initial_state/beads.txt``
    # exists. See for example the previous chapter
    # on Network Assembly how to create that file.
    sim.add_filaments('initial_state/beads.txt')

    sim.run(
        n_steps=100,
        dt=0.005,
        observation_interval=5
    )


class MoleculeSetupHandler(BaseSetupHandler):

    def __init__(self, particle_radius: float):
        self.r = particle_radius

    def __call__(self, system: ReactionDiffusionSystem):
        system.add_topology_species(
            'particle',
            diffusion_constant=0.5/self.r
        )
        system.topologies.add_type('molecule')
        system.topologies.configure_harmonic_bond(
            'particle', 'particle',
            force_constant=50., length=2 * self.r
        )
        for polymer_particle in ['core', 'head', 'tail', 'motor']:
            system.potentials.add_harmonic_repulsion(
                'particle', polymer_particle,
                force_constant=80.,
                interaction_distance=self.r + 0.5  # 0.5 is the radius of
                # the polymer particles.
            )

    def to_config_dict(self) -> Dict[str, Any]:
        return {'particle_radius': self.r}

    def from_config_dict(self, d: Dict[str, float]) -> 'MoleculeSetupHandler':
        return MoleculeSetupHandler(**d)


if __name__ == "__main__":
    run_sim()
