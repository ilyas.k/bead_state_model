
``BaseSetupHandler``
********************

.. autoclass:: bead_state_model.BaseSetupHandler
   :members:

   .. automethod:: __call__
