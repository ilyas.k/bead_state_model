``components``
**************

Users might want to interact with :class:`bead_state_model.components.Bead`
or :class:`bead_state_model.components.Filament` instances
at some point. But usually it should be sufficient to load generate lists of
instances automatically via function :func:`bead_state_model.components.load_beads`,
and hand over what the functions returns to e.g.
:func:`bead_state_model.system_setup.add_filaments`.


.. automodule:: bead_state_model.components
   :members:
