
``Simulation`` and ``SimulationParameters``
*******************************************

.. autoclass:: bead_state_model.SimulationParameters
   :members:

.. autoclass:: bead_state_model.Simulation
   :members:

   .. automethod:: __init__
