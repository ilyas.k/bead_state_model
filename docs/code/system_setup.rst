
``system_setup``
****************

Internally, the most important functions are in module :py:mod:`bead_state_model.system_setup`.
They handle the setup of polymer topologies and polymer reactions via ReaDDy's Python
interface.

.. automodule:: bead_state_model.system_setup
    :members:
