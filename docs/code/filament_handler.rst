``filament_handler``
********************

See :ref:`this page <filamenthandler>` for some reasons why 
the class :class:`bead_state_model.filament_handler.FilamentHandler`
was introduced.

.. automodule:: bead_state_model.filament_handler
    :members:
