For Users: High Level Code
**************************

.. toctree::
   :maxdepth: 1

   simulation
   base_setup_handler
   data_reader
   network_assembly
