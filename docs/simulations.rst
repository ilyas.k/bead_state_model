
.. _Simulations:

Simulations
***********

To run simulations with ``bead_state_model`` you need to
know the basics of the Python programming language.
Simulations have to be started using Python code. To configure
and run simulations, you need to use the classes
:class:`~bead_state_model.SimulationParameters` and
:class:`~bead_state_model.Simulation`.

       
The following example shows you how to use these classes:

.. literalinclude:: _static/simulations/code/run_sim.py
   

So the basic procedure is this:

1. You create a :class:`~bead_state_model.SimulationParameters` object with all the parameters that
   define your system. That have to be at least these 4 parameters:

   .. code::

      parameters = SimulationParameters(
          n_beads_max=20000,
          box_size=np.array([20., 20., 20.]),
          k_bend=26.0,
          k_stretch=20.0,
      )

   Parameter :code:`n_beads_max` is used to internally create an array of appropriate size,
   depending on how many particles you expect in your system. More details
   on that can be found in the documentation of the :class:`~bead_state_model.SimulationParameters`
   class. For now, you can just choose something like 10 times the number of initial particles
   or even larger, the performance should not be impacted by this.
   Parameter :code:`box_size` determines the size of your simulation box, setting up
   periodic boundaries by default.
   Parameters :code:`k_bend` and :code:`k_stretch` define stiffness of the angular and linear
   springs of the bead chains.

2. You create a :class:`~bead_state_model.Simulation` object. The first argument
   specifies the output folder, the second has to be the previously created parameters object:

   .. code::

      sim = Simulation(
          'simulation_output',
          parameters
      )

3. You load an initial state, in the example above, we load it from the file
   ``beads.txt`` in folder ``initial_state``:

   .. code::

      sim.add_filaments('initial_state/beads.txt')

4. You start the simulation with:

   .. code::

      sim.run(
          n_steps=100,
          dt=0.005,
          observation_interval=5
      )

   The snippet above will run the simulation for 100 steps and a time step of 0.005.
   The ``observation_interval`` of 5 leads to 100/5 = 20 recorded frames,
   plus the initial state, leading to 21 frames in total.
   Data will be written to files ``data.h5`` and ``links.h5`` in the folder
   ``simulation_output``.

How to read the simulation output data is demonstrated in the :ref:`Examples`.   

Include External Particles and Potentials
=========================================

The example above contains only polymers. ``bead_state_model`` allows you
to add other external (i.e. non-polymer) particles and external potentials
to your systems. You can make full use of the means of the underlying
`ReaDDy <https://readdy.github.io/>`_ simulation framework to
define particles, define reactions between particles, define energies for
bonds between particles, etc.

To define external components, you have to follow a few rules:

1. You have to define all components in the
   :meth:`~bead_state_model.BaseSetupHandler.__call__` method of an implementation of the
   :class:`~bead_state_model.BaseSetupHandler` class.
2. You have to provide an instance of that class as an argument when you create
   the :class:`~bead_state_model.Simulation` instance for a simulation.
3. To add external particles (in step 1 you abstractly define them, now this step is about
   adding concrete particles at specified locations), you
   have to use the methods :meth:`~bead_state_model.Simulation.add_non_filament_particles`
   and :meth:`~bead_state_model.Simulation.add_non_filament_topology`.

The following short example illustrates these steps by adding a 2-particle molecule
at the center of the simulation box to the example above, that interacts repulsively
with the polymer particles:

.. literalinclude:: _static/simulations/code/run_sim_with_external_topology.py
