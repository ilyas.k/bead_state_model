
Model
=====

Polymers
--------

In ``bead_state_model``, polymers are modeled as chains of beads. An actin filament
(double stranded polymer), is coarse-grained into a small number of beads, where each bead
represents tens to hundreds of G-actin monomers:

.. figure:: ../_static/simulations/model/polymer.svg

   Actin polymer coarse-grained as a bead chain. Springs indicate harmonic interaction
   potentials for distance between beads as well as lateral deflections.

.. _potentials:

Potentials
----------

Within one polymer, beads exert stretching and bending forces on each other. The forces are defined
via harmonic potentials:

.. math::

   U &= U_\text{bend} + U_\text{stretch}\\
   &= \frac{\kappa_b}{2} \sum_{i=1}^{N-2} \theta_i ^2 + \frac{\kappa_s}{2} \sum_{j=1}^{N-1} (l_j - l_0)^2,

where :math:`\theta_i` is the angle between two segments (a segment is the vector connecting two beads),
:math:`l_j` is the length of a segment and :math:`l_0` is the resting length.

.. image:: ../_static/simulations/model/notation_definition.png
   

States
------

Proteins that interact with polymers, like cross-links and myosin motors, are not included
explicitly. Instead, the beads can change their states. Consider two isolated polymers:

.. image:: ../_static/simulations/model/concept_with_symbols_wo_free_1.svg

In their isolated state (i.e. not bound to any other polymer via cross-links or motors),
the first and last bead are in state head and tail, while all beads in between are in state
core.


When two core beads of different filaments are in close proximity, they can change their
state to cross-link:

.. image:: ../_static/simulations/model/concept_with_symbols_wo_free_4.svg

These two beads get connected by a bond (via a harmonic interaction potential), thus forming
a link between the two filaments.

Motor bond formation works in the same way:

.. image:: ../_static/simulations/model/concept_with_symbols_wo_free_5.svg

But motors have the additional ability
to pass their state on along the filament:

.. image:: ../_static/simulations/model/concept_with_symbols_wo_free_6.svg

