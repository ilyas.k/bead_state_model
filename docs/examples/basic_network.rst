Basic Network
=============

This example shows how to generate and then simulate a homogeneous, cross-linked 3D actin
network. Download this example's files
`here <../_static/example_basic_network.zip>`_.

Create Network
--------------

Run the script ``create_network.py``. This creates files with filament coordinates and
parameters into folder ``initial_state``. If you don't change parameters in the script,
it will create 300 filaments consisting of 15 beads each in a simulation box of size
``[20.0, 20.0, 20.0]``.
The class
:class:`~bead_state_model.network_assembly.create_network_simple.CreateNetworkSimple`
handles the network generation. The algorithm is described in section
:ref:`NetworkAssembly`.

Run Simulation
--------------

Run the script ``run_simulation.py``. This will load the initial state, and start
a simulation for 2000 time steps with ``dt = 0.006``
(see :ref:`tune-l-p` how to relate the dimensionless, internal units with physical units).

This simulation might take a few minutes, run time mostly depends on your CPU.
The script will produce 2 output files, ``data.h5`` and ``links.h5``, and put them in folder
``simulation_output``.

The parameter ``rate_motor_bind`` is set to ``2.0``, which means that links will start forming
during the simulation. However, since the parameter ``rate_motor_step`` is set to ``0.0``,
the links do not act as motors, but rather as static cross-links.

Reading the Output Data
-----------------------

3D Visualization
................

The file ``data.h5`` is a regular readdy trajectory file, and we can use readdy's export
method to export it to ``xyz`` file format:

.. code::

   import readdy
   
   traj = readdy.Trajectory('simulation_output/data.h5')
   traj.convert_to_xyz(generate_tcl=False)

This creates the file ``simulation_output/data.h5.xyz``. I use ovito for visualization:

.. image:: ../_static/example_basic_network/xyz_with_readdy.png

The box is rather crowded, and you won't be able to see much. It's a crowded box
of particles of different types. The section :ref:`visualization` of the example
:ref:`tune-l-p` shows how to use the package ``actomyosin_analyser`` to produce
visualizations with a tube representation:

.. raw:: html

   <video width="400" height="400" controls>
   <source src="../_static/example_basic_network/animation.mp4" type="video/mp4">
   Your browser does not support the video tag.
   </video>

In the video, green fibers are actin filaments, red tubes are cross-links between filaments.
Only a slice of width 5 is shown.

Reading Polymer Coordinates
...........................

``bead_state_model`` contains all the tools you need to read coordinates and the information
which bead belongs to which polymer and in which order:

.. code::

   from bead_state_model.data_reader import DataReader
   
   dr = DataReader('simulation_output/data.h5')
   coordinates = dr.read_particle_positions()
   filaments = dr.get_filaments_all()

   print(coordinates.shape)
   # prints (21, 4500, 3), if you did not change the default number
   # of simulation steps / write frequency or number of
   # polymers / beads per polymer.
   # First axis is number of frames,
   # second axis is number of beads,
   # third is number of dimensions.

   print(len(filaments))
   # prints 21, one item per frame.
   print(len(filaments[0]))
   # prints 300, one item per polymer
   print(filaments[0][0])
   # Information which beads belong to this filament,
   # we will use them later to select coordinates
   # from our (21, 4500, 3) array.

   # Let's plot a few, randomly selected polymers at the
   # first frame in their 2D projection.
   import matplotlib.pyplot as plt
   import numpy as np

   # select 10 indices
   selected_indices = np.random.choice(np.arange(len(filaments[0])), 10)

   for idx in selected_indices:
       c_idx = coordinates[0, filaments[0][idx].items]
       x = c_idx[:, 0]
       y = c_idx[:, 1]
       plt.plot(x, y, '-o')

   plt.xlabel('$x/x_0$')
   plt.ylabel('$y/x_0$')
       
   plt.show()

The plot should look similar to the following one. Yours might have some artifacts
due to lines spanning across the periodic boundaries.
   
.. image:: ../_static/example_basic_network/10_filaments.svg
   
Reading Coordinates with ``actomyosin_analyser``
................................................

Although it probably did not show in this small example, reading the data is very
computationally and memory expensive in general. For larger output files, you
should use the package ``actomyosin_analyser``
(`project <https://gitlab.com/ilyas.k/actomyosin_analyser>`_,
`documentation <http://akbg.uni-goettingen.de/docs/actomyosin_analyser/>`_)
rather than with tools provided in
``bead_state_model``. The class ``actomyosin_analyser.analysis.analyser.Analyser``
interfaces with the class :class:`~bead_state_model.data_reader.DataReader`,
and reads the raw data (coordinates and filaments) only once. The ``Analyser`` will save it
in a format that is much quicker and uses way less memory upon reading. If you need the
data again, it will be read much quicker the second time.

Here is the same example again, this time using the methods of the ``Analyser`` class:

.. code::

   import matplotlib.pyplot as plt
   import numpy as np

   from bead_state_model.data_reader import DataReader
   from actomyosin_analyser.analysis.analyser import Analyser
   
   dr = DataReader('simulation_output/data.h5')
   analyser = Analyser(dr, 'simulation_output/analysis.h5')
   coordinates = analyser.get_trajectories_filaments()
   filaments = analyser.get_filaments()

   # select 10 indices
   selected_indices = np.random.choice(np.arange(len(filaments[0]), 10))

   for idx in selected_indices:
       c_idx = coordinates[0, filaments[0][idx].items]
       x = c_idx[:, 0]
       y = c_idx[:, 1]
       plt.plot(x, y, '-o')

   plt.xlabel('$x/x_0$')
   plt.ylabel('$y/x_0$')
       
   plt.show()
   
.. _length-dist:

Length Distributions
--------------------

To illustrate that the initial state (generated with the
create_network script) does not generate fully equilibrated filaments,
the example files include a script ``plot_length_distributions.py``.
It will produce a figure like this:

.. figure:: ../_static/example_basic_network/distributions.svg

   (top) Bending and stretching energies plotted against the frame number.
   Energies are initially lower than during simulation.
   (bottom) Gaussian kernel density estimates of the segment
   length distributions (distance between neighbouring beads of a
   filament) at frame 0 (equivalent to the initial state) and at
   the last recorded frame 20.

The bending and stretching energies are initially lower than during simulation. The initial angles
and distances between beads are drawn from normal distributions that, for simplicity,
do not take 3D coordinates and repulsion between neighbours into account. In the simulation,
however, both these aspects play an important role in shaping the angle and length
distributions. In the plot of the length distributions, it is clearly visible, that
the lengths are initialized (frame 0) with mean :math:`1 x_0`, where :math:`x_0` is
the diameter of a bead. However, due to repulsion between these beads during simulation,
overlap is penalized and the distribution quickly shifts to a skewed distribution
with a mean :math:`\mu > 1`.
