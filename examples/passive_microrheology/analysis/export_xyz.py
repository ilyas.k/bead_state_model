from pathlib import Path
import readdy


def main():
    file_data = _get_file_path(sim_index=0)

    traj = readdy.Trajectory(file_data)
    traj.convert_to_xyz(generate_tcl=False)


def _get_file_path(sim_index: int) -> str:
    """
    Try to find data file either from current folder (i.e. '.')
    or from the parent folder. This makes execution of the script a bit
    more flexible, as it can be run from its folder (`analysis`) or from the
    root folder (the folder that contains `analysis`).

    :return: Path to data file.
    """
    p = Path('.')
    file_path = p / 'simulations' / f"out{sim_index:04}" / "data.h5"
    if not file_path.is_file():
        file_path = p.parent / 'simulations' / f"out{sim_index:04}" / "data.h5"
    return str(file_path)


if __name__ == '__main__':
    main()
