import os
import sys
import time
from multiprocessing import Pool
from typing import Dict, Any, Tuple

import numpy as np
import toml

from bead_state_model import SimulationParameters, Simulation

kernel = 'SingleCPU'


def main(n_proc: int):
    if not os.path.exists('simulations'):
        os.mkdir('simulations')
    sim_indices = list(range(120))
    pool = Pool(n_proc, initializer=_mute)
    pool.map(configure_and_run_sim, sim_indices)


def _mute():
    sys.stdout = open(os.devnull, 'w')


def configure_and_run_sim(idx: int) -> Tuple[float, float]:
    """
    Run one simulation with given parameters. Simulation is split into parts,
    to allow viewing interim results of the simulation and make it possible 
    to continue simulation from a check point.

    :param idx: Index of simulation to run.
    :return: Run time for simulation measured with time.time and time.process_time.
    """

    file_params = 'initial_states/filament{:04}/params.toml'.format(idx)

    with open(file_params, 'rt') as fh:
        params = toml.load(fh)

    params['out_dir'] = 'simulations/out{:04}'.format(idx)
    params['index'] = idx

    print('running simulation {:04}'.format(idx))
    t_start = time.time()
    pt_start = time.process_time()

    run_sim(params)

    t_end = time.time()
    pt_end = time.process_time()

    t = t_end - t_start
    pt = pt_end - pt_start

    print('done with simulation {}'.format(idx))

    return t, pt


def run_sim(params: Dict[str, Any]):
    sim_idx = params['index']
    obs_interval = params['run']['observation_interval']

    folder_output = params['out_dir']
    os.makedirs(folder_output, exist_ok=True)

    folder_filament = 'initial_states/filament{:04}'.format(sim_idx)

    sys_params = params['system']
    box = np.array([sys_params['box_size']['x'],
                    sys_params['box_size']['y'],
                    sys_params['box_size']['z']])
    k_bend = sys_params['k_bend'] / 2
    k_stretch = sys_params['k_stretch'] / 2
    dt = params['run']['dt']

    sim_params = SimulationParameters(
        box_size=box,
        k_bend=k_bend,
        k_stretch=k_stretch,
        n_beads_max=int(2e2)
    )
    file_beads = os.path.join(folder_filament, 'beads.txt')

    sim = Simulation(folder_output, sim_params, kernel)
    sim.add_filaments(file_beads)

    sim.run(n_steps=params['run']['n_steps'], dt=dt, observation_interval=obs_interval)


if __name__ == "__main__":
    # adjust n_proc, number of processes to run in parallel
    main(n_proc=4)
