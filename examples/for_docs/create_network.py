import numpy as np
from bead_state_model.network_assembly.create_network_simple import CreateNetworkSimple

system = CreateNetworkSimple(
    box=np.array([20., 20., 20.]),
    k_bend=26.0,
    k_stretch=20.0,
    n_filaments=100,
    n_beads_per_filament=10
)

system.run(verbose=True)
system.write('initial_state')