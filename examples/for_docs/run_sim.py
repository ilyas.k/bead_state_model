import numpy as np
from bead_state_model import Simulation, SimulationParameters

parameters = SimulationParameters(
    n_beads_max=20000,
    box_size=np.array([20., 20., 20.]),
    k_bend=26.0,
    k_stretch=20.0,
)

sim = Simulation(
    'simulation_output',
    parameters
)

# Requires that the file ``initial_state/beads.txt``
# exists. See for example the previous chapter
# on Network Assembly how to create that file.
sim.add_filaments('initial_state/beads.txt')

sim.run(
    n_steps=100,
    dt=0.005,
    observation_interval=5
)