import os
import time
import numpy as np
from bead_state_model.network_assembly.create_network_simple \
    import CreateNetworkSimple


os.makedirs('initial_state', exist_ok=True)
np.random.seed(np.uint32(hash(str(time.time()))))

"""
CreateNetworkSimple generates random entangled networks,
i.e. filaments without any cross-links or motors.
Reduce the number of filaments and/or beads if you want to 
speed up the network assembly and the simulation.
"""

n_filaments = 300
n_beads_per_filament = 15

# size of simulation BOX
box = np.array([20., 20., 20.])

# parameters for bending (determines persistence length) and 
# stretching between beads
k_bend = 26.0
k_stretch = 20.0

system = CreateNetworkSimple(
    box=box,
    k_bend=k_bend,
    k_stretch=k_stretch,
    n_filaments=n_filaments,
    n_beads_per_filament=n_beads_per_filament
)

system.run(verbose=True)
system.write(folder='initial_state')
print("done")
