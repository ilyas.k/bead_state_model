PACKAGE_VERSION=$(shell cat setup.cfg | grep version | cut -d'=' - -f2 | xargs)
PACKAGE_ROOT=dist/bead_state_model-$(PACKAGE_VERSION)

build:
	python -m build

upload:
	echo $(PACKAGE_VERSION)
	twine upload $(PACKAGE_ROOT).tar.gz $(PACKAGE_ROOT)-py3-none-any.whl

test:
	pytest --disable-warnings -q tests/unit
	pytest --disable-warnings -q tests/integration
	pytest --disable-warnings -q tests/acceptance
	pytest --disable-warnings -q tests/examples
